module.exports = {
  DB_SAVE_ERROR: "Failed to Save in Database",
  EXCEPTION: "An Exception Occured",
  DATA_MISSING: "Required Data Missing",

  LOGIN: "Login Successful",
  SIGNUP: "Signup Successful",
  SIGNUP_ERROR: "Account Not Created",

  INVALID: "Invalid Email or Password",

  USER_EXIST: "User Already Exists",
  USER_NOT_FOUND: "Unable to find user",

  RECORD_UPDATED: "Record Updated Successfully",
  RECORD_DELETED: "Record Deleted Successfully",

  RECORD_FOUND: "Record Found Successfully",
  RECORD_NOT_FOUND: "Record Not Found",
};
