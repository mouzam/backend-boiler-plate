"use strict";

var exports = {};

exports.setup = function(app) {
  console.log("Setting up routes.");

  var admin = require("./routes/adminRoutes");

  app.use("/admin/api/v1/", admin);
};

module.exports = exports;
