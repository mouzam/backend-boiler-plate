"use strict";

// Constants
const constants = require("../../utilities/appConstants");
const messageConstants = require("../../utilities/messageConstants");
const responseService = require("../../services/responseService");

const {
  login,
  isExist,
  signUp,
} = require("../../../src/resolvers/adminResolver");

class AdminController {
  async login(data) {
    if (!data.email || !data.password) {
      return responseService.createResponse(
        constants.RESPONSE_STATUS.ERROR,
        data,
        messageConstants.DATA_MISSING
      );
    }
    try {
      const user = await login(data);

      if (user)
        return responseService.createResponse(
          constants.RESPONSE_STATUS.SUCCESS,
          { user: user },
          messageConstants.LOGIN
        );
      else {
        return responseService.createResponse(
          constants.RESPONSE_STATUS.Error,
          messageConstants.RECORD_NOT_FOUND,
          messageConstants.INVALID
        );
      }
    } catch (error) {
      return responseService.createResponse(
        constants.RESPONSE_STATUS.ERROR,
        error,
        messageConstants.EXCEPTION
      );
    }
  }

  async signUp(data) {
    if (!data.email || !data.password) {
      return responseService.createResponse(
        constants.RESPONSE_STATUS.ERROR,
        data,
        messageConstants.DATA_MISSING
      );
    }

    try {
      // Check Already Exist or Not
      const userExist = await isExist(data);

      if (userExist) {
        return responseService.createResponse(
          constants.RESPONSE_STATUS.ERROR,
          data,
          messageConstants.USER_EXIST
        );
      }
      // SignUp

      const user = await signUp(data);

      if (user)
        return responseService.createResponse(
          constants.RESPONSE_STATUS.SUCCESS,
          { user: user },
          messageConstants.SIGNUP
        );
      else {
        return responseService.createResponse(
          constants.RESPONSE_STATUS.ERROR,
          data,
          messageConstants.SIGNUP_ERROR
        );
      }
    } catch (error) {
      return responseService.createResponse(
        constants.RESPONSE_STATUS.ERROR,
        error,
        messageConstants.EXCEPTION
      );
    }
  }
}

var exports = (module.exports = new AdminController());
