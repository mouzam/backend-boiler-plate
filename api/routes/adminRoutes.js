"use strict";

var router = require("express").Router();
var adminController = require("../controllers/admin/adminController");

router.post("/login", function (req, res) {
  res.json(adminController.login(req.body));
});

router.post("/signUp", function (req, res) {
  res.json(adminController.signUp(req.body));
});

module.exports = router;
