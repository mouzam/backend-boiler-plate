// Constants
const constants = require("../utilities/appConstants");

class ResponseService {
  async createResponse(status, responseData, message) {
    console.log(
      "TCL: ResponseService -> createSuccessResponse -> responseData",
      responseData
    );
    let responseObj;

    if (status === "SUCCESS") {
      responseObj = {
        metadata: {
          status: status,
          message: message,
          responseCode: constants.STATUS_CODE.SUCCESS.OK,
        },
        payload: {
          data: responseData,
        },
      };
    } else if (status === "FAILURE") {
      responseObj = {
        metadata: {
          status: status,
          message: message,
          responseCode: constants.STATUS_CODE.SUCCESS.OK,
        },
        payload: {
          data: responseData,
        },
      };
    } else {
      responseObj = {
        metadata: {
          status: status,
          message: message,
          responseCode: constants.STATUS_CODE.SUCCESS.OK,
        },
        payload: {
          data: responseData,
        },
      };
    }

    return responseObj;
  }

  createErrorResponse(responseData) {}
  createExceptionResponse(responseData) {}
}

var exports = (module.exports = new ResponseService());
