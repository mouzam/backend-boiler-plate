module.exports = `
  type Admin {
    email: String!  
    password: String!
    added_by: String
    added_date: String
  }

  type Query {
    admin: [Admin]
  }

  type Mutation {
    login(
        email: String!,
        password: String!,
        added_by: String,
        added_date: String,
        ): Admin
}`;
