const { mergeTypes } = require("merge-graphql-schemas");
const adminType = require("./Admin");
const customerType = require("./Customer");
const types = [adminType, customerType];

module.exports = mergeTypes(types, { all: true });
