module.exports = `
  type Customer {
    name: String
    email: String!  
    phone:String
    added_by: String
    added_date: String
  }

  type Query {
    customer: [Customer]
  }

  type Mutation {
    addCustomer(
        name:String,
        email: String!,
        phone: String,
        added_by: String,
        added_date: String,
        ): Customer
}`;
