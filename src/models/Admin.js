const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const adminSchema = new Schema({
  email: {
    type: String,
    index: true,
    required: "Please Enter Email"
  },
  password: {
    type: String,
    required: "Please Enter Password"
  },

  added_by: {
    type: String
  },
  added_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("Admin", adminSchema);
