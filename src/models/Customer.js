const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const customerSchema = new Schema({
  name: {
    type: String
  },
  email: {
    type: String,
    index: true,
    required: "Please Enter Email"
  },
  phone: {
    type: String
  },

  added_by: {
    type: String
  },
  added_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("Customer", customerSchema);
