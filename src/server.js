const express = require("express");
const { buildSchema } = require("graphql");

const graphqlHTTP = require("express-graphql");
const typeDefs = require("./schemas/index");
const rootValue = require("./resolvers/index");

bodyParser = require("body-parser");

const schema = buildSchema(typeDefs);

var mongoose = require("mongoose");
const app = express();
var port = process.env.PORT || 3000;

mongoose.Promise = global.Promise;

const connectionString =
  "mongodb+srv://boiler:ABqirzbq4GVntL7G@boilerplate-pdug5.mongodb.net/test?retryWrites=true&w=majority";

mongoose.connect(connectionString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

mongoose.connection.once("open", () => {
  console.log("Connected to database successfully");
});

app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    rootValue,
    graphiql: true,
  })
);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(require("express-promise")());

var cors = require("cors");

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  next();
});

app.use(cors());
//importing routes

var routes = require("../api/routes");

//register the route
routes.setup(app);

var http = require("http").Server(app);

http.listen(port);

console.log("Server running on port: " + port);
