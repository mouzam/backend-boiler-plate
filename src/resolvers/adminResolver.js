const Admin = require("../models/Admin");

module.exports = {
  // Query

  isExist: data => {
    return Admin.findOne({ email: data.email }).then(user => {
      return user;
    });
  },

  login: data => {
    return Admin.findOne({ email: data.email, password: data.password }).then(
      user => {
        return user;
      }
    );
  },

  //Mutation
  signUp: data => {
    let admin = new Admin(data);
    return admin.save();
  }
};
