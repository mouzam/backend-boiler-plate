const { mergeResolvers } = require("merge-graphql-schemas");

const adminResolver = require("./adminResolver");
const customerResolver = require("./customerResolver");

const resolvers = [adminResolver, customerResolver];

module.exports = mergeResolvers(resolvers);
